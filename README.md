# OpenML dataset: Bank-Marketing-Dataset

https://www.openml.org/d/43718

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Find the best strategies to improve for the next marketing campaign. How can the financial institution have a greater effectiveness for future marketing campaigns? In order to answer this, we have to analyze the last marketing campaign the bank performed and identify the patterns that will help us find conclusions in order to develop future strategies.
Source
[Moro et al., 2014] S. Moro, P. Cortez and P. Rita. A Data-Driven Approach to Predict the Success of Bank Telemarketing. Decision Support Systems, Elsevier, 62:22-31, June 2014

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43718) of an [OpenML dataset](https://www.openml.org/d/43718). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43718/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43718/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43718/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

